﻿using Advanced.CarDealer.Domain.Infraestructure.Interface.DataContextQuery;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Advanced.CarDealer.Infraestructure.Interface.DataContextQuery
{
    public class CarDealerDbContextQuery : ICarDealerDbContextQuery
    {
        private readonly DbConnection Connection;
        private readonly bool CloseConnection;

        public CarDealerDbContextQuery(DbConnection connection, bool closeConnection = true)
        {
            this.Connection = connection;
            this.CloseConnection = closeConnection;
        }

        public async Task<T> QueryAsync<T>(string sql, object parameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var connectionOpened = await CheckConnection(cancellationToken);

            try
            {
                return (await Connection.QueryAsync<T>(sql, parameters)).FirstOrDefault();
            }
            finally
            {
                CloseIf(connectionOpened);
            }
        }
        public async Task<IEnumerable<T>> QueryListAsync<T>(string sql, object parameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var connectionOpened = await CheckConnection(cancellationToken);

            try
            {
                return await this.Connection.QueryAsync<T>(sql, parameters);
            }
            finally
            {
                CloseIf(connectionOpened);
            }
        }
        public async Task<IEnumerable<T3>> QueryMap<T1, T2, T3>(string sql, Func<T1, T2, T3> map, object parameters = null, string splitOn = "Id", CancellationToken cancellationToken = default(CancellationToken))
        {
            var connectionOpened = await CheckConnection(cancellationToken);

            try
            {
                return await this.Connection.QueryAsync(sql, map, parameters, null, true, splitOn);
            }
            finally
            {
                CloseIf(connectionOpened);
            }
        }
        private async Task<bool> CheckConnection(CancellationToken cancellationToken)
        {
            if (this.Connection.State != ConnectionState.Closed)
            {
                return false;
            }

            await this.Connection.OpenAsync(cancellationToken);

            return true;
        }
        private void CloseIf(bool connectionOpened)
        {
            if (connectionOpened && this.CloseConnection)
            {
                this.Connection.Close();
            }
        }
    }
}