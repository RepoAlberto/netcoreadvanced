﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Advanced.CarDealer.Infraestructure.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Core");

            migrationBuilder.EnsureSchema(
                name: "Master");

            migrationBuilder.CreateSequence<int>(
                name: "HiloCarSecuence",
                incrementBy: 10);

            migrationBuilder.CreateSequence<int>(
                name: "HiloDealerSecuence",
                incrementBy: 10);

            migrationBuilder.CreateSequence<int>(
                name: "HiloSaleSecuence",
                incrementBy: 10);

            migrationBuilder.CreateSequence<int>(
                name: "HiloSalesManSecuence",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Dealers",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Email = table.Column<string>(maxLength: 320, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 5, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Rating = table.Column<int>(nullable: false, defaultValue: 1),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Salesmans",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Salesmans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EngineTypes",
                schema: "Master",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EngineTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Engines",
                schema: "Master",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TotalHorsePower = table.Column<int>(nullable: false),
                    EngineTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Engines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Engines_EngineTypes_EngineTypeId",
                        column: x => x.EngineTypeId,
                        principalSchema: "Master",
                        principalTable: "EngineTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    DealerId = table.Column<int>(nullable: false),
                    EngineId = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cars_Dealers_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "Core",
                        principalTable: "Dealers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cars_Engines_EngineId",
                        column: x => x.EngineId,
                        principalSchema: "Master",
                        principalTable: "Engines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    OriginalPrice = table.Column<decimal>(type: "decimal(13,2)", nullable: false),
                    FinalPrice = table.Column<decimal>(nullable: false),
                    Rating = table.Column<decimal>(nullable: false),
                    CarId = table.Column<int>(nullable: true),
                    SalesmanId = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sales_Cars_CarId",
                        column: x => x.CarId,
                        principalSchema: "Core",
                        principalTable: "Cars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sales_Salesmans_SalesmanId",
                        column: x => x.SalesmanId,
                        principalSchema: "Core",
                        principalTable: "Salesmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "Master",
                table: "EngineTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Eléctrico" },
                    { 2, "Híbrido" },
                    { 3, "Diesel" },
                    { 4, "Gasolina" }
                });

            migrationBuilder.InsertData(
                schema: "Master",
                table: "Engines",
                columns: new[] { "Id", "EngineTypeId", "Name", "TotalHorsePower" },
                values: new object[,]
                {
                    { 5, 1, "2.57 (rthyfj)", 250 },
                    { 4, 2, "1.3 (defsdf)", 200 },
                    { 3, 3, "3.8 6l (D545415)", 400 },
                    { 1, 4, "5.0 V10 (S85B50)", 507 },
                    { 2, 4, "3.2 6l (S54B32HP)", 320 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cars_DealerId",
                schema: "Core",
                table: "Cars",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_EngineId",
                schema: "Core",
                table: "Cars",
                column: "EngineId");

            migrationBuilder.CreateIndex(
                name: "IX_Dealers_Name",
                schema: "Core",
                table: "Dealers",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sales_CarId",
                schema: "Core",
                table: "Sales",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_SalesmanId",
                schema: "Core",
                table: "Sales",
                column: "SalesmanId");

            migrationBuilder.CreateIndex(
                name: "IX_Engines_EngineTypeId",
                schema: "Master",
                table: "Engines",
                column: "EngineTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Engines_Name",
                schema: "Master",
                table: "Engines",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EngineTypes_Name",
                schema: "Master",
                table: "EngineTypes",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sales",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "Cars",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "Salesmans",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "Dealers",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "Engines",
                schema: "Master");

            migrationBuilder.DropTable(
                name: "EngineTypes",
                schema: "Master");

            migrationBuilder.DropSequence(
                name: "HiloCarSecuence");

            migrationBuilder.DropSequence(
                name: "HiloDealerSecuence");

            migrationBuilder.DropSequence(
                name: "HiloSaleSecuence");

            migrationBuilder.DropSequence(
                name: "HiloSalesManSecuence");
        }
    }
}
