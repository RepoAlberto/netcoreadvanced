﻿
using Advanced.CarDealer.Domain.Model.CarDealer;
using Advanced.CarDealer.Infraestructure.EntityConfiguration;
using Advanced.CarDealer.Infraestructure.EntityConfiguration.Master;
using Microsoft.EntityFrameworkCore;
using System;

namespace Advanced.CarDealer.Infraestructure
{
    public static class DataContextExtensions
    {
        public static ModelBuilder ApplyEntityDateStamp<T>(this ModelBuilder modelBuilder) where T : class//, IDateAuditable
        {
            modelBuilder.Entity<T>().Property<DateTime>("CreationDate")
                .HasColumnType("datetime2");

            modelBuilder.Entity<T>().Property<DateTime>("UpdatedDate")
                .HasColumnType("datetime2");

            return modelBuilder;
        }

        public static ModelBuilder ApplyEntityConfgurations(this ModelBuilder modelBuilder) =>
            modelBuilder.ApplyConfiguration(new CarEntityConfiguration())
                        .ApplyConfiguration(new DealerEntityConfiguration())
                        .ApplyConfiguration(new SaleEntityConfiguration())
                        .ApplyConfiguration(new SalesManEntityConfiguration())
                        .ApplyConfiguration(new EngineTypeEntityConfiguration())
                        .ApplyConfiguration(new EngineEntityConfiguration());

        public static ModelBuilder ApplyDateStamp(this ModelBuilder modelBuilder) =>
            modelBuilder.ApplyEntityDateStamp<Car>()
                        .ApplyEntityDateStamp<Dealer>()
                        .ApplyEntityDateStamp<Sale>()
                        .ApplyEntityDateStamp<Salesman>();


        public static ModelBuilder ApplySecuences(this ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasSequence<int>(Secuences.HiloCarSecuence)
                .StartsAt(1)
                .IncrementsBy(10);

            modelBuilder
                .HasSequence<int>(Secuences.HiloDealerSecuence)
                .StartsAt(1)
                .IncrementsBy(10);

            modelBuilder
                .HasSequence<int>(Secuences.HiloSaleSecuence)
                .StartsAt(1)
                .IncrementsBy(10);

            modelBuilder
                .HasSequence<int>(Secuences.HiloSalesManSecuence)
                .StartsAt(1)
                .IncrementsBy(10);

            return modelBuilder;
        }
    }
}