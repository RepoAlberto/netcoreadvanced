﻿using Advanced.CarDealer.Domain.Model.CarDealer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration
{
    public class SalesManEntityConfiguration : IEntityTypeConfiguration<Salesman>
    {
        public void Configure(EntityTypeBuilder<Salesman> builder)
        {
            builder
                .ToTable(Tables.Salesmans, Schemas.Core);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Id)
                .UseHiLo(Secuences.HiloSalesManSecuence);

            builder
                .HasMany(e => e.Sales)
                .WithOne(e => e.Salesman)
                .HasForeignKey(e => e.SalesmanId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}