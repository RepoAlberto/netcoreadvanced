﻿using Advanced.CarDealer.Domain.Model.CarDealer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration
{
    public class CarEntityConfiguration : IEntityTypeConfiguration<Car>
    {
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder
                .ToTable(Tables.Cars, Schemas.Core);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Id)
                .UseHiLo(Secuences.HiloCarSecuence);

            builder.
                Property(e => e.Name)
                .HasMaxLength(DefaultValues.DefaultStringMaxLength);

            builder
                .HasOne(e => e.Dealer)
                .WithMany(e => e.Cars)
                .HasForeignKey(e => e.DealerId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder
                .HasOne(e => e.Engine)
                .WithMany()
                .HasForeignKey(e => e.EngineId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
