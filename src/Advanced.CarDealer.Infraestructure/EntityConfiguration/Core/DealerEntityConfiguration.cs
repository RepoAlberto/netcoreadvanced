﻿using Advanced.CarDealer.Domain.Model.CarDealer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration
{
    public class DealerEntityConfiguration : IEntityTypeConfiguration<Dealer>
    {
        public void Configure(EntityTypeBuilder<Dealer> builder)
        {
            builder
                .ToTable(Tables.Dealers, Schemas.Core);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Id)
                .UseHiLo(Secuences.HiloDealerSecuence);
            
            builder
                .Property(e => e.Name)
                .HasMaxLength(DefaultValues.DefaultStringMaxLength)
                .IsRequired();

            builder
                .HasIndex(e => e.Name)
                .IsUnique();

            builder
                .Property(e => e.Email)
                .HasMaxLength(DefaultValues.EmailDefaulStringLength);

            builder
                .Property(e => e.PostalCode)
                .HasMaxLength(5);

            builder
                .Property(e => e.Rating)
                .HasDefaultValue(1);
        }
    }
}