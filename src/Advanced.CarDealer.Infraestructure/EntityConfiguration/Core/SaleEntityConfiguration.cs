﻿using Advanced.CarDealer.Domain.Model.CarDealer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration
{
    public class SaleEntityConfiguration : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.ToTable(Tables.Sales, Schemas.Core);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Id)
                .UseHiLo(Secuences.HiloSaleSecuence);

            builder
                .Property(e => e.OriginalPrice)
                .HasColumnType("decimal(13,2)");

            builder
                .HasOne(e => e.Salesman)
                .WithMany(e => e.Sales)
                .HasForeignKey(e => e.SalesmanId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder
                .HasOne(e => e.Car)
                .WithMany(e => e.Sales)
                .HasForeignKey(e => e.CarId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
