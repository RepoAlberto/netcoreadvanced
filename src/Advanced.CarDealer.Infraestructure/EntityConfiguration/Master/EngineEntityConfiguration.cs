﻿using Advanced.CarDealer.Domain.Model.CarDealer.Master;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration
{
    public class EngineEntityConfiguration : IEntityTypeConfiguration<Engine>
    {
        public void Configure(EntityTypeBuilder<Engine> builder)
        {
            builder
                .ToTable(Tables.Engines, Schemas.Master);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Name)
                .HasMaxLength(DefaultValues.DefaultStringMaxLength)
                .IsRequired();

            builder
                .HasIndex(e => e.Name)
                .IsUnique();

            builder.HasOne(e => e.EngineType)
                .WithMany()
                .HasForeignKey(e => e.EngineTypeId)
                .IsRequired();

            builder.HasData(new List<Engine>()
            {
                new Engine() { Id = 1, Name = "5.0 V10 (S85B50)", EngineTypeId = EngineType.Gasoline.Id, TotalHorsePower = 507 },
                new Engine() { Id = 2, Name = "3.2 6l (S54B32HP)", EngineTypeId = EngineType.Gasoline.Id, TotalHorsePower = 320 },
                new Engine() { Id = 3, Name = "3.8 6l (D545415)", EngineTypeId = EngineType.Diesel.Id, TotalHorsePower = 400 },
                new Engine() { Id = 4, Name = "1.3 (defsdf)", EngineTypeId = EngineType.Hybrid.Id, TotalHorsePower = 200 },
                new Engine() { Id = 5, Name = "2.57 (rthyfj)", EngineTypeId = EngineType.Electric.Id, TotalHorsePower = 250 }
            });
        }
    }
}
