﻿using Advanced.CarDealer.Domain.Model.CarDealer.Master;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Advanced.CarDealer.Infraestructure.EntityConfiguration.Master
{
    public class EngineTypeEntityConfiguration : IEntityTypeConfiguration<EngineType>
    {
        public void Configure(EntityTypeBuilder<EngineType> builder)
        {
            builder
                .ToTable(Tables.EngineTypes, Schemas.Master);

            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Name)
                .HasMaxLength(DefaultValues.DefaultStringMaxLength)
                .IsRequired();

            builder
                .HasIndex(e => e.Name)
                .IsUnique();

            builder.HasData(EngineType.GetAll());
        }
    }
}
