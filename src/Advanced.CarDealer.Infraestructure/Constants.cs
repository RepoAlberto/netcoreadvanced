﻿namespace Advanced.CarDealer.Infraestructure
{
    public static class Schemas
    {
        internal const string Core = nameof(Core);
        internal const string Master = nameof(Master);
    }

    public static class Tables
    {
        internal const string Dealers = nameof(Dealers);
        internal const string Cars = nameof(Cars);
        internal const string Salesmans = nameof(Salesmans);
        internal const string Sales = nameof(Sales);
        internal const string Colors = nameof(Colors);
        internal const string Engines = nameof(Engines);
        internal const string EngineTypes = nameof(EngineTypes);
    }

    public static class DefaultValues
    {
        internal const int DefaultStringMaxLength = 255;
        internal const int EmailDefaulStringLength = 320;
    }

    public static class Secuences
    {
        internal const string HiloCarSecuence = nameof(HiloCarSecuence);
        internal const string HiloDealerSecuence = nameof(HiloDealerSecuence);
        internal const string HiloSaleSecuence = nameof(HiloSaleSecuence);
        internal const string HiloSalesManSecuence = nameof(HiloSalesManSecuence);
    }
}