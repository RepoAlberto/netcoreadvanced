﻿using Advanced.CarDealer.Domain.Model.CarDealer;
using Advanced.CarDealer.Domain.Model.CarDealer.Master;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Advanced.CarDealer.Infraestructure.DataContext
{
    public class CarDealerContext: DbContext
    {
        public CarDealerContext(DbContextOptions<CarDealerContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Dealer> Dealers { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Salesman> Salesman { get; set; }
        public virtual DbSet<Engine> Engine { get; set; }
        public virtual DbSet<EngineType> EngineType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplySecuences()
                .ApplyEntityConfgurations()
                .ApplyDateStamp();

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
