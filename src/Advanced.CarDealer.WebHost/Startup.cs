using Advanced.CarDealer.Api;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Advanced.CarDealer.WebHost
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) =>
            services.ConfigureServices(configuration);

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) =>
            Configuration.Configure(app, env);
    }
}
