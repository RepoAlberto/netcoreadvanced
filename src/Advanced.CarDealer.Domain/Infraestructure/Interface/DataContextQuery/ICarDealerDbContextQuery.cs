﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Advanced.CarDealer.Domain.Infraestructure.Interface.DataContextQuery
{
    public interface ICarDealerDbContextQuery
    {
        Task<T> QueryAsync<T>(string sql, object parameters = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<T>> QueryListAsync<T>(string sql, object parameters = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<T3>> QueryMap<T1, T2, T3>(string sql, Func<T1, T2, T3> map, object parameters = null, string splitOn = "Id", CancellationToken cancellationToken = default(CancellationToken));
    }
}