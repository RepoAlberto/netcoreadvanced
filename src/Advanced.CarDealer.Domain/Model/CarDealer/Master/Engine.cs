﻿namespace Advanced.CarDealer.Domain.Model.CarDealer.Master
{
    public class Engine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalHorsePower { get; set; }

        public int EngineTypeId { get; set; }
        public EngineType EngineType { get; set; }
    }
}