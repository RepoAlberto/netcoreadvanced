﻿using System.Collections.Generic;
using System.Linq;

namespace Advanced.CarDealer.Domain.Model.CarDealer.Master
{
    public class EngineType
    {
        public static readonly EngineType Electric = new EngineType(1, "Eléctrico");
        public static readonly EngineType Hybrid = new EngineType(2, "Híbrido");
        public static readonly EngineType Diesel = new EngineType(3, "Diesel");
        public static readonly EngineType Gasoline = new EngineType(4, "Gasolina");

        public int Id { get; private set; }
        public string Name { get; private set; }

        public EngineType()
        { }

        public EngineType(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public EngineType GetById(int id)
        {
            var current = GetAll().FirstOrDefault(e => e.Id == id);

            if (current == null)
                throw new System.ArgumentException("EngineType not found");   

            return current;
        }

        public static List<EngineType> GetAll() =>
            new List<EngineType>()
            {
                Electric,
                Hybrid,
                Diesel,
                Gasoline
            };
    }
}