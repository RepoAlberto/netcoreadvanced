﻿namespace Advanced.CarDealer.Domain.Model.CarDealer
{
    public class Sale 
    {
        public int Id { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal FinalPrice { get; set; }
        public decimal Rating { get; set; }

        public int? CarId { get; set; }
        public Car Car { get; set; }

        public int SalesmanId { get; set; }
        public Salesman Salesman { get; set; }
    }
}