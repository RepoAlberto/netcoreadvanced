﻿using System.Collections.Generic;

namespace Advanced.CarDealer.Domain.Model.CarDealer
{
    public class Dealer 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public int Rating { get; set; }

        /// <summary>
        /// Relacion con ventas
        /// </summary>
        internal List<Car> cars = new List<Car>();
        public IReadOnlyCollection<Car> Cars => cars.AsReadOnly();
    }
}
