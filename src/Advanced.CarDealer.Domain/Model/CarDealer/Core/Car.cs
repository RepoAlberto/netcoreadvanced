﻿using Advanced.CarDealer.Domain.Model.CarDealer.Master;
using System.Collections.Generic;

namespace Advanced.CarDealer.Domain.Model.CarDealer
{
    public class Car 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public int DealerId { get; set; }
        public Dealer Dealer { get; set; }

        public int? EngineId { get; set; }
        public Engine Engine { get; set; }

        /// <summary>
        /// Relacion con ventas
        /// </summary>
        internal List<Sale> sales = new List<Sale>();
        public IReadOnlyCollection<Sale> Sales => sales.AsReadOnly();
    }
}