﻿using System.Collections.Generic;

namespace Advanced.CarDealer.Domain.Model.CarDealer
{
    public class Salesman 
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Relacion con ventas
        /// </summary>
        internal List<Sale> sales = new List<Sale>();
        public IReadOnlyCollection<Sale> Sales => sales.AsReadOnly();
    }
}