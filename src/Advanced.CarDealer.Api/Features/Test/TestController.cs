﻿using Advanced.CarDealer.Api.Features.Test;
using Advanced.CarDealer.Api.Features.Test.Get;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Advanced.CarDealer.Api.Features
{
    [Route("/api/test")]
    public class TestController : ControllerBase
    {
        public TestController()
        {

        }

        /// <summary>
        /// Documentación del endpoint de Hola mundo
        /// </summary>
        /// <param name="getHelloTestModel">Documentación del modelo de entrada del ep de Hola Mundo</param>
        /// <returns>Documentación del modelo de salida del ep de Hola Mundo</returns>
        [HttpGet("say-hello")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetHelloTestModelResponse))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> HelloWorld(GetHelloTestModel getHelloTestModel)
        {
            return Ok(new GetHelloTestModelResponse(getHelloTestModel.Name));
        }
    }
}