﻿using FluentValidation;

namespace Advanced.CarDealer.Api.Features.Test.Get
{
    public class GetHelloTestValidator : AbstractValidator<GetHelloTestModel>
    {
        public GetHelloTestValidator()
        {
            RuleFor(model => model)
                .NotNull()
                .NotEmpty();

            RuleFor(model => model.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage("El nombre es requerido");
        }
    }
}