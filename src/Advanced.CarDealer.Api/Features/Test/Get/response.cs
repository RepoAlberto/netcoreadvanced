﻿namespace Advanced.CarDealer.Api.Features.Test
{
    public class GetHelloTestModelResponse
    {
        private readonly string name = string.Empty;

        public GetHelloTestModelResponse(string name)
        {
            this.name = name;
        }

        public string HelloText { get { return $"Hola {this.name} y a todos los participantes del curso de net core para iniciados!!!"; } }
    }
}