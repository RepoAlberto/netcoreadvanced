﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Advanced.CarDealer.Api
{
    public static class ProblemDetailsExceptionMapping
    {
        internal static ProblemDetails FluentValidationHandler(FluentValidationException fluentValidationException) =>
            new ProblemDetails()
            {
                Detail = fluentValidationException.Message,
                Status = StatusCodes.Status400BadRequest,
                Title = "Validation errors"
            };
    }
}
