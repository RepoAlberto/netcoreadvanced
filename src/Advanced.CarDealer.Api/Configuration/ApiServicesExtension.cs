﻿using Advanced.CarDealer.Api.Features.Test.Get;
using Advanced.CarDealer.Domain.Infraestructure.Interface.DataContextQuery;
using Advanced.CarDealer.Infraestructure.DataContext;
using Advanced.CarDealer.Infraestructure.Interface.DataContextQuery;
using FluentValidation.AspNetCore;
using Hellang.Middleware.ProblemDetails;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;

namespace Advanced.CarDealer.Api
{
    public static class ApiServicesExtension
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {

            return services;
        }

        public static IServiceCollection AddCustomContexts(this IServiceCollection services, IConfiguration configuration) =>
            services.AddDbContext<CarDealerContext>(opt => opt.UseSqlServer(configuration["connectionStrings:carDealer"]))
                    .AddTransient<ICarDealerDbContextQuery, CarDealerDbContextQuery>(opts => new CarDealerDbContextQuery(new SqlConnection(configuration["connectionStrings:carDealer"])));

        public static IServiceCollection AddOpenApi(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration) =>
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "V0",
                    Title = "Advanced Car Dealer",
                    Description = "Advanced Car Dealer Open Api (ASP.NET Core 3.1)",
                    Contact = new OpenApiContact()
                    {
                        Name = "Swagger Codegen Contributors",
                        Url = new Uri("https://github.com/swagger-api/swagger-codegen"),
                        Email = ""
                    },
                });

                options.DescribeAllParametersInCamelCase();
                options.CustomSchemaIds(t => t.FullName);
                options.IncludeXmlComments($"{AppContext.BaseDirectory}Advanced.CarDealer.WebHost.xml");
            });

        public static IMvcBuilder AddCustomFluentValidation(this IMvcBuilder services) =>
            services.AddFluentValidation(cfg => cfg.RegisterValidatorsFromAssemblyContaining<GetHelloTestValidator>());


        public static IServiceCollection AddCustomProblemDetails(this IServiceCollection services)
        {
            services.AddProblemDetails(opts =>
            {
                opts.IncludeExceptionDetails = (ctx, ex) => {
                    var env = ctx.RequestServices.GetRequiredService<IHostEnvironment>();
                    return env.IsDevelopment() || env.IsStaging();
                    //return false;
                };

                opts.Map<FluentValidationException>(ex => ProblemDetailsExceptionMapping.FluentValidationHandler(ex));
            });

            return services;
        }
    }
}