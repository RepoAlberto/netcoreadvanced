﻿using Advanced.CarDealer.Infraestructure.DataContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Advanced.CarDealer.Api
{
    public class HttpActionFilter : IAsyncActionFilter  
    {
        private readonly CarDealerContext carDealerContext;

        public HttpActionFilter(CarDealerContext carDealerContext) =>
            this.carDealerContext = carDealerContext;

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // Antes de la ejecucion del controlador
            if (!context.ModelState.IsValid)
            {
                var error = new FluentValidationException();
                var modelErrors = context
                                    .ModelState
                                    .Where(e => e.Value.Errors.Count > 0)
                                    .ToDictionary(ky => ky.Key, val => val.Value.Errors.Select(e => e.ErrorMessage).ToList());

                foreach (var fieldErrors in modelErrors)
                {
                    foreach (var errorValue in fieldErrors.Value)
                    {
                        error.AddError(fieldErrors.Key, errorValue);
                    }
                }

                error.Build();
            }

            var executionResult = await next();

            // Despues de la ejecucion del controlador














            if (context.HttpContext.Request.Method != HttpMethods.Get && executionResult.Exception == null)
                await this.carDealerContext.SaveChangesAsync(default(CancellationToken));
        }
    }
}