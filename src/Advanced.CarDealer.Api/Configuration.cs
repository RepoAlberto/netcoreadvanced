﻿using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Advanced.CarDealer.Api
{
    public static class Configuration
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration) =>
            services
                    .AddCustomContexts(configuration)
                    .AddCustomServices()
                    .AddCustomProblemDetails()
                    .AddOpenApi(configuration)
                    .AddControllers(opt =>
                    {
                        opt.Filters.Add<HttpActionFilter>();
                    })
                    .AddCustomFluentValidation()
                .Services;

        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger()
               .UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "Advanced Car Dealer API");
               })
               .UseProblemDetails()
               .UseHttpsRedirection()
               .UseRouting()
               .UseAuthorization()
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllers();
               });
        }
    }
}
