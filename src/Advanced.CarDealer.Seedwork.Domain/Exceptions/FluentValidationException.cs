﻿using System;

namespace System
{
    public class FluentValidationException : Exception
    {
        private string ErrorDescription = "";

        public FluentValidationException()
        {

        }
        public FluentValidationException(string message) : base(message)
        {
        }

        public void AddError(string field, string errorDescription)
        {
            if (!string.IsNullOrEmpty(this.ErrorDescription))
                this.ErrorDescription += Environment.NewLine;

            this.ErrorDescription += $"{field}: {errorDescription}";
        }

        public void Build() =>
            throw new FluentValidationException(this.ErrorDescription);
    }
}
